# ROS package for simulations at CS with Turtlebots 3

- consensus
- formation control (leader-follower)

Assuming you are working on the virtual machine provided by CS

## install turtlebot3 package

`sudo apt install ros-noetic-turtlebot3`

## How to build this simulation
Open the terminal (ctrl + alt + t)
```
$ mkdir -p ~/catkin_ws/src
$ cd ~/catkin_ws/src
$ git clone https://gitlab-research.centralesupelec.fr/aarsh.thakker/tb3_sim_cs_3a.git
$ cd ~/catkin_ws
$ ros1ws
$ catkin_init_workspace
$ catkin build
```

## Giving permission to execute the python script
```
$ cd ~/catkin_ws/src/tb3_sim_cs_3a/scripts
$ chmod +x tp3a_multi_ctrl_cart_vel.py tp3a_algos.py pose_to_odom.py si_to_uni.py 
```

## How to clean the workspace in case of any error
```
$ cd ~/catkin_ws
$ catkin clean -y
```

## How to run simulation for leader follower
You only need to change your algorithm in the `scripts/tp3a_algos.py` and uncomment the required controller in the end of the file.

Open the terminal (ctrl + alt + t)
```
$ cd ~/catkin_ws
$ ros1ws
$ roslaunch tb3_sim_cs_3a gazebo_5_tb.launch
```
In another terminal
```
$ cd ~/catkin_ws
$ ros1ws
$ roslaunch tb3_sim_cs_3a simu_tp3a_tb.launch no_of_robots:=5
```

## How to run simulation for consensus
You only need to change your algorithm in the `scripts/tp3a_algos.py` and uncomment the required controller in the end of the file.

Open the terminal (ctrl + alt + t)
```
$ cd ~/catkin_ws
$ ros1ws
$ roslaunch tb3_sim_cs_3a gazebo_4_tb.launch
```
In another terminal
```
$ cd ~/catkin_ws
$ ros1ws
$ roslaunch tb3_sim_cs_3a simu_tp3a_tb.launch no_of_robots:=4
```
